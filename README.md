<!--
SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
SPDX-License-Identifier: CC0-1.0
-->

# F-Droid Mointor Deployment

scripts for deplyoing https://gitlab.com/fdroid/fdroid-monitor

```
ansible-galaxy install -f -r requirements.yml -p .galaxy
ansible-playbook -i fdroidMonitor, webserver.yml
ansible-playbook -i fdroidMonitor, buildinfo.yml
```
